package com.ashokit.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StudentComposite implements Serializable {

	private Integer rollNo;
	@Column(length = 12)
	private String section;

	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	public StudentComposite(Integer rollNo, String section) {
		super();
		this.rollNo = rollNo;
		this.section = section;
	}
	
	public StudentComposite() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
